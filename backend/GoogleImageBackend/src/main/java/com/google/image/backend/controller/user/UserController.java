package com.google.image.backend.controller.user;

import com.google.image.backend.entity.user.User;
import com.google.image.backend.other.RoleDto;
import com.google.image.backend.service.user.UserService;
import com.google.image.backend.session.user.UserSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;

@Controller
public class UserController {
    private final UserService userService;
    private final UserSession userSession;
    @Value("${admin.secret.key}")
    private String adminSecretKey;
    @Autowired
    public UserController(UserService userService,UserSession userSession){
        this.userService = userService;
        this.userSession = userSession;
    }
    @PostMapping("/main/login")
    public RedirectView loginUser(@RequestParam(name="login")String login,
                                  @RequestParam(name="password")String password){
        RedirectView redirectView = new RedirectView("/login");
        User user = User.builder().login(login).password(password).build();
        if(userService.checkUsersLoginAndPassword(user)){
            userSession.setLogin(login);
            userSession.setId(user.getId());
            userSession.setRole(user.getRole());
            userSession.setPassword(user.getPassword());
            redirectView.setUrl("/");
        }
        return redirectView;
    }
    @GetMapping("main/role")
    public @ResponseBody
    RoleDto getRole(){
        RoleDto roleDto = new RoleDto();
        roleDto.setRole("");
        if(userSession!=null){
            roleDto.setRole(userSession.getRole());
        }
        return roleDto;
    }
    @PostMapping("main/register")
    public RedirectView registerUser(@RequestParam(name="login")String login,
                        @RequestParam(name="password")String password,
                        @RequestParam(name="role")String role){
        RedirectView redirectView = new RedirectView("register");
        User user = User.builder().login(login).password(password).role(role).build();
        if(role.equals("admin")){
            userSession.setLogin(login);
            userSession.setPassword(password);
            redirectView.setUrl("admin_confirmation");
        }else if(userService.registerUser(user)){
            userSession.setLogin(user.getLogin());
            userSession.setId(user.getId());
            userSession.setRole(user.getRole());
            userSession.setPassword(user.getPassword());
            redirectView.setUrl("search");
        }
        return redirectView;
    }
    @PostMapping("main/admin_confirmation")
    public RedirectView confirmAdmin(@RequestParam(name="adminSecretKey") String adminSecretKey){
        RedirectView redirectView = new RedirectView("register");
        User user = User.builder().login(userSession.getLogin()).
                password(userSession.getPassword()).
                role("admin").build();
        if(this.adminSecretKey.equals(adminSecretKey)&&userService.registerUser(user)){
            userSession.setId(user.getId());
            userSession.setRole(user.getRole());
            redirectView.setUrl("image_adder");
        }
        return redirectView;
    }
}
