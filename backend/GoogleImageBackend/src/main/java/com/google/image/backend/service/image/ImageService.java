package com.google.image.backend.service.image;

import com.google.image.backend.entity.image.Image;
import com.google.image.backend.repository.image.IImageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ImageService {
    private final IImageRepository imageRepository;
    public Image findImageByName(String name){
        return imageRepository.findByName(name);
    }
    public Image findImageByPath(String path){
        return imageRepository.findByPath(path);
    }
    public List<Image> findImageByKeyWordsContaining(String keyWords){
        return imageRepository.findByKeyWordsIsContaining(keyWords);
    }
    public void saveImage(String name,String keyWords, String path){
        if(findImageByPath(path)==null){
            Image image = Image.builder().name(name).path(path).keyWords(keyWords).build();
            imageRepository.save(image);
        }
    }
    @Transactional
    public boolean deleteImageByName(String name){
        long result = imageRepository.deleteByName(name);
        return result>0;
    }
    @Transactional
    public void updateImageByName(String name, String keyWords, String path){
        imageRepository.updateByName(name,keyWords,path);
    }
    @Transactional
    public void updateImageKeyWordsByName(String name, String keyWords){
        imageRepository.updateKeyWordsByName(name,keyWords);
    }
    @Transactional
    public void updateImagePathByName(String name, String path){
        imageRepository.updatePathByName(name,path);
    }
}
