package com.google.image.backend.entity.image;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
public class Image {
    @Id
    @GeneratedValue
    private Long id;
    @Column
    private String name;
    @Column
    private String path;
    @Column
    private String keyWords;
}