package com.google.image.backend.service.user;

import com.google.image.backend.entity.user.User;
import com.google.image.backend.repository.user.IUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {
    private final IUserRepository userRepository;
    private final BCryptPasswordEncoder passwordEncoder;
    synchronized public boolean registerUser(User user){
        boolean result = false;
        if(userRepository.findByLogin(user.getLogin())==null){
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            User tempUser = userRepository.save(user);
            user.setId(tempUser.getId());
            result = true;
        }
        return result;
    }
    public boolean checkUsersLoginAndPassword(User user){
        boolean result = false;
        User tempUser = userRepository.findByLogin(user.getLogin());
        if(tempUser!=null&&passwordEncoder.matches(user.getPassword(), tempUser.getPassword())){
            user.setRole(tempUser.getRole());
            user.setId(tempUser.getId());
            result = true;
        }
        return result;
    }
    public User findUserByLogin(String login){
        return userRepository.findByLogin(login);
    }
}
