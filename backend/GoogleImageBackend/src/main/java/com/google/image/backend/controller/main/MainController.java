package com.google.image.backend.controller.main;

import com.google.image.backend.session.user.UserSession;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.view.RedirectView;

import java.io.FileInputStream;
import java.io.InputStream;

@Controller
public class MainController {
    private final UserSession userSession;
    @Value("${default.page.image.folder}")
    private String defaultPageImageFolder;
    @Autowired
    public MainController(UserSession userSession){
        this.userSession = userSession;
    }

    @GetMapping("style/{fileName}")
    public String style(@PathVariable String fileName){
        return "style/"+fileName;
    }

    @GetMapping("script/{fileName}")
    public String script(@PathVariable String fileName){
        return "script/" + fileName;
    }

    @GetMapping("/")
    public RedirectView main(){
        String result = "main/";
        if(userSession.getLogin()==null){
            result+="login";
        }else if(userSession.getRole().equals("admin")){
            result+="image_adder";
        }else{
            result+="search";
        }
        return new RedirectView(result);
    }
    @GetMapping("/page_images/{image}")
    public @ResponseBody
    byte[] getPageImage(@PathVariable("image")String image)throws Exception{
        InputStream inputStream = new FileInputStream(defaultPageImageFolder+"//"+image);
        return IOUtils.toByteArray(inputStream);
    }

    @GetMapping("/main/{page}")
    public String page(@PathVariable String page){
        return "body/"+page;
    }
}
