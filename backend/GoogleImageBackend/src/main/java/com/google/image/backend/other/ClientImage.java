package com.google.image.backend.other;

import lombok.*;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ClientImage {
    private String name;
    private String keyWords;
    private byte[] bytes;
}
