package com.google.image.backend.session.user;

import lombok.*;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

@SessionScope
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@Builder
@Component
public class UserSession {
    private Long id;
    private String login;
    private String password;
    private String role;
}
