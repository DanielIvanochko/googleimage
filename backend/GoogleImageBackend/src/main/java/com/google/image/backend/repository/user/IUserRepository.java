package com.google.image.backend.repository.user;

import com.google.image.backend.entity.user.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUserRepository extends JpaRepository<User,Long> {
    User findByLogin(String login);
}
