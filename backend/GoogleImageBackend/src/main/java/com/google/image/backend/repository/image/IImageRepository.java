package com.google.image.backend.repository.image;
import com.google.image.backend.entity.image.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface IImageRepository extends JpaRepository<Image, Long> {
    Image findByName(String name);
    Image findByPath(String path);
    @Query(value="SELECT i FROM Image i where i.keyWords LIKE %:keyWords%")
    List<Image> findByKeyWordsIsContaining(@Param("keyWords") String keyWords);
    @Modifying
    long deleteByName(String name);
    @Modifying
    @Query(value="update Image set keyWords=:keyWords, path=:path where name=:name")
    void updateByName(@Param("name") String name,@Param("keyWords") String keyWords,
            @Param("path") String path);
    @Modifying
    @Query(value = "update Image set keyWords=:keyWords where name=:name")
    void updateKeyWordsByName(@Param("name") String name,@Param("keyWords") String keyWords);
    @Modifying
    @Query(value = "update Image set path=:path where name=:name")
    void updatePathByName(@Param("name")String name, @Param("path") String path);
}
