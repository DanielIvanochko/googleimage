package com.google.image.backend.controller.image;

import com.google.image.backend.entity.image.Image;
import com.google.image.backend.other.ClientImage;
import com.google.image.backend.service.image.ImageService;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.view.RedirectView;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

@Controller
public class ImageController {
    private final ImageService imageService;
    @Value("${default.image.folder}")
    private String imageFolder;
    @Autowired
    public ImageController(ImageService imageService){
        this.imageService = imageService;
    }
    @GetMapping("main/image")
    public @ResponseBody
    List<ClientImage> findImageByKeyWords(@RequestParam(name="keyWords")String keyWords)throws Exception{
        List<Image> images = imageService.findImageByKeyWordsContaining(keyWords);
        ArrayList<ClientImage> clientImages = new ArrayList<>();
        for(Image image: images){
            InputStream inputStream = new FileInputStream(image.getPath());
            clientImages.add(new ClientImage(image.getName(),image.getKeyWords(), IOUtils.toByteArray(inputStream)));
        }
        return clientImages;
    }
    @PostMapping(value="upload",
    consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public RedirectView
    uploadImage(@RequestParam("name")String name,
                @RequestParam("keyWords")String keyWords,
                @RequestParam(name="image") MultipartFile imageFile)throws IOException{
        if(!imageFile.isEmpty()){
            String fileName = StringUtils.cleanPath(imageFile.getOriginalFilename());
            imageService.saveImage(name,keyWords,imageFolder+"/"+fileName);
            imageFile.transferTo(new File(imageFolder+"/"+fileName));
            System.out.println(fileName);
        }
        return new RedirectView("main/image_adder");
    }
    @DeleteMapping("main/image/{name}")
    public ResponseEntity<String> deleteImage(@PathVariable String name)throws Exception{
        Image image =  imageService.findImageByName(name);
        ResponseEntity<String> responseEntity = ResponseEntity.notFound().build();
        if(image!=null){
            String path = image.getPath();
            if(imageService.deleteImageByName(name)){
                System.out.println("File deleted from db");
                //Files.deleteIfExists(Paths.get(path));
                responseEntity = ResponseEntity.ok().build();
            }
        }
        return responseEntity;
    }

    @PostMapping(value="main/update",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public RedirectView updateImage(@RequestParam("name")String name,
                                              @RequestParam("keyWords")String keyWords,
                                              @RequestParam(name="image") @Nullable MultipartFile imageFile)
    throws IOException{
        System.out.println("In Method:"+name+", "+keyWords);
        Image image = imageService.findImageByName(name);
        if(!keyWords.isEmpty()&&image!=null){
            String path = image.getPath();
            if(imageFile!=null&&!imageFile.isEmpty()){
                path = imageFolder+"/"+StringUtils.cleanPath(imageFile.getOriginalFilename());
                imageFile.transferTo(new File(path));
                System.out.println(path);
            }
            imageService.updateImageByName(name,keyWords,path);
        }
        return new RedirectView("search");
    }
    @PutMapping(value="main/image/keyWords")
    public ResponseEntity<String> updateImageKeyWords(@RequestParam("name") String name,
                                                      @RequestParam("keyWords") String keyWords){
        ResponseEntity<String> responseEntity = ResponseEntity.notFound().build();
        Image image = imageService.findImageByName(name);
        if(image!=null){
            imageService.updateImageKeyWordsByName(name,keyWords);
            responseEntity = ResponseEntity.ok().build();
        }
        return responseEntity;
    }
    @PutMapping(value="main/image/file",
            consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<String> updateImagePath(@RequestParam("name") String name,
                                                  @RequestParam("image") MultipartFile imageFile)
    throws Exception
    {
        ResponseEntity<String> responseEntity = ResponseEntity.notFound().build();
        if(imageFile!=null&&!imageFile.isEmpty()&&imageFile.getOriginalFilename()!=null){
            String path = imageFolder+"/"+ StringUtils.cleanPath(imageFile.getOriginalFilename());
            imageFile.transferTo(new File(path));
            imageService.updateImagePathByName(name,path);
            responseEntity = ResponseEntity.ok().build();
        }
        return responseEntity;
    }
}
