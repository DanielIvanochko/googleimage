function Register(){
  return(
    <div className="register-page">
      <Logo/>
      <Form/>
    </div>
  );
}
function Form(){
  return(
    <div className="register-div">
      <form className="register-form" action="register" method="POST" name="register">
        <p className="form-name">Sign up</p>
        <input type="text" name="login" required="required" placeholder="Login" id="login" className="input"/>
        <hr/>
        <input type="password" name="password" required="required" placeholder="Password" 
        maxLength="16" id="password" className="input"/>
        <hr/>
        <input type="password" name="password2" required="required" placeholder="Confirm password"
        maxLength="16" id="password2" className="input"/>
        <hr/>
        <div className="register-as">
          <p className="radio-role">Sign up as</p>
          <input type="radio" name="role" id="user" value="user" className="radio" checked/>
          <label htmlFor="user">User</label>
          <br/>
          <input type="radio" name="role" id="admin" value="admin" className="radio" />
          <label htmlFor="admin">Admin</label>
        </div>
        <button id="submit" type="submit" value="sign-up" name="submitButton" className="submit">Sign up</button>
        <a className="login-link" href="login">Log in</a>
      </form>
    </div>
  );
}
function Logo(){
  return(
    <div className="logo">
      <h1 className="logo-google">Google<span className="logo-image">Image</span></h1>
    </div>
  )
}

ReactDOM.render(<Register/>, document.getElementById("root"));