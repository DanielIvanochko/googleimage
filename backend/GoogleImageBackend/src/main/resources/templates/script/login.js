function Login(){
  return(
    <div className="login-page">
      <Logo/>
      <Form/>
    </div>
  );
}
function Form(){
  return(
    <div className="login-div">
      <form className="login-form" action="login" method="POST" name="login">
        <p className="form-name">Log in</p>
        <input type="text" name="login" required="required" placeholder="Login" id="login" className="input"/>
        <hr/>
        <input type="password" name="password" required="required" placeholder="Password"
        maxLength="16" id="password" className="input"/>
        <hr/>
        <button id="submit" type="submit" value="log-in" name="submitButton" className="submit">Log in</button>
        <a href="register" className="register-link">Register</a>
        </form>
    </div>
  );
}
function Logo(){
  return(
    <div className="logo">
      <h1 className="logo-google">Google<span className="logo-image">Image</span></h1>
    </div>
  )
}

ReactDOM.render(<Login/>, document.getElementById("root"));