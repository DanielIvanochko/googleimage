var role="user";
var json={};
function getRole(){
  const http = new XMLHttpRequest();
  const url="role";
  http.open("GET",url);
  http.send(null);
  http.onreadystatechange = (e) => {
    if(http.readyState==4&&http.status==200){
      let jsonRole = JSON.parse(http.responseText);
      role=jsonRole["role"];
      console.log(role);
    }
  }    
}
getRole();

function Search(){
  return(
    <div className="search-page" >
      <Logo/>
      <SearchLine/>
    </div>
  );
}
function SearchLine(){
  return(
    <div className="search-div">
      <input type="text" name="keyWords" required="required" placeholder="Search" id="keyWords" className="input"/>
      <button id="searchButton" type="submit" value="search" name="submitButton" className="submit" 
      onClick={getImage}>
      <img src="../page_images/search_icon.png" width="43px" height="43px" alt="search icon"/>
      </button>
    </div>
  );
}
function Logo(){
  return(
    <div className="logo">
      <h1 className="logo-google">Google<span className="logo-image">Image</span></h1>
    </div>
  )
}

function SearchResults(){
  return(
    <div className="search-res"  >
        <Header/>
        <Results/>
        <PhotoDiv/>
        <EditingForm/>
    </div>
  );
}

function Header(){
  return(
    <div className="search-header">
      <LogoSmall/>
      <SearchLineSmall/>
      <Logout/>
    </div>
  )
}

function LogoSmall(){
  return(
    <div className="logo-small">
      <h1 className="logo-google-small">Google<span className="logo-image-small">Image</span></h1>
    </div>
  )
}

function SearchLineSmall(){
  return(
    <div className="search-div-small">
      <input type="text" name="keyWords" required="required" placeholder="Search" id="keyWords" className="input"/>
      <button id="searchButton" type="submit" value="search" name="submitButton" className="submit" 
      onClick={getImage}>
      <img src="../page_images/search_icon.png" width="43px" height="43px" alt="search icon"/>
      </button>
    </div>
  );
}

function Logout(){
  return(
    <div className="logout-div">
      <a href="login">
        <img src="../page_images/logout_icon.png" width="43px" height="43px"/>
      </a>
    </div>
  );
}


function PhotoDiv(){
  return(
    <div className="photo-div" id="photo-div" onClick={closeViewer}>
      <img className="photo" src="" id ="photo"/>
    </div>
  )
}
function PhotoView(){
  let photoDiv=document.getElementById("photo-div");
  photoDiv.style.display="block";
}
function closeViewer(){
  let photoDiv=document.getElementById("photo-div");
  photoDiv.style.display="none";
}

const searchButtonId = "searchButton";
function changeRoot(){
  let root=document.getElementById('root');
  root.style.justifyContent="start";
  root.style.alignItems="flex-start";  
}

function showImage(event){
  let img=document.getElementById("photo");
  img.src=event.target.src;
  let photoDiv=document.getElementById("photo-div");
  photoDiv.style.display="flex";
}
function openEditor(event){
  document.getElementById("edit-file-div").style.display="flex";
  let divID=event.currentTarget.id;
  divID=divID.slice(1);
  let imgIndex=Number(divID);
  console.log(json);
  document.getElementById("name").value=json[imgIndex]["name"];
  document.getElementById("keyWordsInput").value=json[imgIndex]["keyWords"];
  document.getElementById("editedImage").src="data:image/png;base64,"+json[imgIndex]["bytes"];
  console.log(json[imgIndex]["keyWords"]);
  //document.getElementById("chooseFile").value="data:image/png;base64,"+json[imgIndex]["bytes"];
  document.getElementById("edit-file-div").style.display="flex";
}
var Results=null;
function setImages(json){
  var images=[];
  for(let i = 0;i<json.length;i++) {
    let img=React.createElement("img", {src:"data:image/png;base64,"+json[i]["bytes"], className:"res-img",
    onClick:showImage,id:json[i]["name"], onLoad:closeViewer, onLoad:cancelEditing});
    console.log(role);
    if(role==="admin"){
      let info=[];
      info.push(img);
      let editIcon=React.createElement("img",{width:"20px", height:"20px", src:"../page_images/edit_icon.png"},);
      let deleteIcon=React.createElement("img",{width:"20px", height:"20px", src:"../page_images/delete_icon.png"},);
      let editButton=React.createElement("button",{id:"e"+i, className:"actionButton editButton", type:"submit",name:"actionButton", onClick:(e)=>{openEditor(e)}} , editIcon);
      let deleteButton=React.createElement("button",{id:"d"+i, className:"actionButton deleteButton", type:"submit",name:"actionButton", onClick:event=>deleteImage(event)}, deleteIcon);
      info.push(editButton);
      info.push(deleteButton);
      images.push(React.createElement("div", { className: "result-image", id:"result-image"+i }, info));
    }else{
      images.push(React.createElement("div", { className: "result-image", id:"result-image"+i }, img));
    }
    Results=()=>React.createElement("div", { className: "results" , id:"results"}, images);
   }
  // for(let i=1;i<=8;i++){
  //   let img=React.createElement("img", {src:"../page_images/image"+i+".jpg", className:"res-img",
  //   onClick:showImage, onLoad:closeViewer, onLoad:cancelEditing});
  //   if(role==="admin"){
  //     let info=[];
  //     info.push(img);
  //     let editIcon=React.createElement("img",{width:"20px", height:"20px", src:"../page_images/edit_icon.png"},);
  //     let deleteIcon=React.createElement("img",{width:"20px", height:"20px", src:"../page_images/delete_icon.png"},);
  //     let editButton=React.createElement("button",{id:"e"+i, className:"actionButton editButton", type:"submit",name:"actionButton", onClick:(e)=>{openEditor(e)}} , editIcon);
  //     let deleteButton=React.createElement("button",{id:"d"+i, className:"actionButton deleteButton", type:"submit",name:"actionButton", onClick:event=>deleteImage(event)}, deleteIcon);
  //     info.push(editButton);
  //     info.push(deleteButton);
  //     images.push(React.createElement("div", { className: "result-image", id:"result-image"+i }, info));
  //   }else{
  //     images.push(React.createElement("div", { className: "result-image", id:"result-image"+i }, img));
  //   }
  //   Results=()=>React.createElement("div", { className: "results" , id:"results"}, images);
  // }
}
function showPhoto(event){
  let img=document.getElementById("editedImage");
  img.src = URL.createObjectURL(event.target.files[0]);
    img.onload = function() {
      URL.revokeObjectURL(img.src) 
    }
}
function deleteImage(event){
  let divID=event.currentTarget.id;
  console.log(divID);
  divID=divID.slice(1);
  console.log(divID);
  let imgIndex=Number(divID);
  document.getElementById("results").childNodes[imgIndex].style.display="none";
  const http = new XMLHttpRequest();
  const url="image/"+json[imgIndex]["name"];
  http.open("delete",url);
  http.send(null);
}


function EditingForm(){
  return(
    <div className="edit-file-div" id="edit-file-div">
      <form action="update" method="post" encType="multipart/form-data" className="edit-file-form" id="edit-file-form">
        <p className="form-name">Edit image information</p>
        <input type="text" name="name" id="name" placeholder="Image name" className="input edit" required="required" readOnly/>
        <hr/>
        <input type="text" name="keyWords" id="keyWordsInput" placeholder="Image keywords" className="input edit" required="required"/>
        <hr/>
        <input type="file" id="chooseFile" name="image" accept="image/png, image/jpeg" className="button" onChange={showPhoto}/>
        <img src="" className="editedImage" id="editedImage"/>
        <div className="buttons">
          <button type="submit" className="button">Submit</button>
          <button type="button" className="button" onClick={cancelEditing}>Cancel</button>
        </div>
      </form>
    </div>
  )
}

function cancelEditing(){
  document.getElementById('edit-file-form').reset();
  document.getElementById('editedImage').src="";
  document.getElementById('edit-file-div').style.display="none";
}
function getImage()
{
  const http = new XMLHttpRequest();
  const url="image?keyWords="+document.getElementById("keyWords").value;
  http.open("GET",url);
  http.send(null);
  http.onreadystatechange = (e) => {
    if(http.readyState==4&&http.status==200){
      json = JSON.parse(http.responseText);
      setImages(json);
    }else{
      Results=()=>React.createElement("div", { className: "results", onLoad:{closeViewer}}, <h3 className="warning">Nothing was found</h3>);
      //setImages();
    }
    changeRoot();
    ReactDOM.render(<SearchResults/>, document.getElementById("root"));
  }    
}
  
ReactDOM.render(<Search/>, document.getElementById("root"));