function Confirmation(){
  return(
    <div className="admin-confirmation-page">
      <Logo/>
      <Form/>
    </div>
  );
}
function Form(){
  return(
    <div className="admin-confirmation-div">
      <form className="admin-confirmation-form" action="admin_confirmation" method="POST" name="admin-confirmation-form">
        <p className="form-name">Admin confirmation</p>
        <input type="password"  id="adminSecretKey" name="adminSecretKey" placeholder="Admin secret key"
        className="input"/>
        <hr/>
        <button id="submit" type="submit" value="enter" name="submitButton" className="submit">Enter</button>
        <a className="register-link" href="register">Register</a>
      </form>
    </div>
  );
}
function Logo(){
  return(
    <div className="logo">
      <h1 className="logo-google">Google<span className="logo-image">Image</span></h1>
    </div>
  )
}

ReactDOM.render(<Confirmation/>, document.getElementById("root"));