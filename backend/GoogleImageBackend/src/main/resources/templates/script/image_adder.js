function AddFile(){
  return(
    <div className="add-file-page">
      <Logo/>
      <Form/>
    </div>
  );
}
function Form(){
  return(
    <div className="add-file-div">
      <form action="/upload" method="post" encType="multipart/form-data" className="add-file-form">
        <p className="form-name">Add image</p>
        <input type="text" name="name" id="name" placeholder="Image name" className="input" required="required"/>
        <hr/>
        <input type="text" name="keyWords" id="keyWords" placeholder="Image keywords" className="input" required="required"/>
        <hr/>
        <input type="file" name="image" accept="image/png, image/jpeg" className="submit" onChange={showPhoto}/>
        <img src="" className="photo" id="photo"/>
        <button type="submit" className="submit">Send image</button>
        <a href="search" className="search-link">Search</a>
      </form>
    </div>
  );
}
function Logo(){
  return(
    <div className="logo">
      <h1 className="logo-google">Google<span className="logo-image">Image</span></h1>
    </div>
  )
}


function showPhoto(event){
  let img=document.getElementById("photo");
  img.src = URL.createObjectURL(event.target.files[0]);
    img.onload = function() {
      URL.revokeObjectURL(img.src) 
    }
}
ReactDOM.render(<AddFile/>, document.getElementById("root"));