package com.google.image.backend;

import com.google.image.backend.entity.user.User;
import com.google.image.backend.service.user.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class GoogleClientImageBackendApplicationTests {
    private final UserService userService;

    @Autowired
    public GoogleClientImageBackendApplicationTests(UserService userService){
        this.userService = userService;
    }

    @Test
    void contextLoads() {
    }
    @Test
    void testUserRegisterWithUserService(){
        User user = User.builder().login("hello").password("1234").role("user").build();
        Assertions.assertTrue(userService.registerUser(user));
    }
    @Test
    void testUserLoginWithUserService(){
        User user = User.builder().login("hello").password("12345").role("user").build();
        Assertions.assertFalse(userService.checkUsersLoginAndPassword(user));
    }
}
